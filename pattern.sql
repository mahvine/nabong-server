INSERT INTO public.pattern (pattern_regex,pattern_type,field_names,page_owner_id,field_mapping,"createdAt","updatedAt") VALUES
	 ('cash in|cashin','CASH IN','{"username","gcash"}',NULL,'{
 "username":"user name:?|username:?",
 "gcash":"gcash:|bank:"
}','2021-07-18 17:15:17.886487+08','2021-07-18 17:15:17.886487+08'),
	 ('cash out|cashout','CASH OUT','{"username","bank","account number","account name","amount"}',NULL,'{
 "username":"user name:?|username:?",
 "bank":"bank to deposit:|bank:",
 "account name":"Account name:|Account:",
 "account number":"Account no:|Account number:",
 "amount":"Amount:"
}','2021-07-18 16:17:50.521+08','2021-07-18 16:17:50.521+08'),
	 ('transfer','TRANSFER','{"from","to","amount"}',NULL,'{
 "from":"username from:|User name from:",
 "to":"username to:|User name to:",
 "amount":"amount:"
}','2021-07-18 18:05:25.246197+08','2021-07-18 18:05:25.246197+08');