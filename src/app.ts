import path from 'path';
import favicon from 'serve-favicon';
import compress from 'compression';
import helmet from 'helmet';
import cors from 'cors';

import feathers from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';
import express from '@feathersjs/express';
import socketio from '@feathersjs/socketio';
import sslRedirect from 'heroku-ssl-redirect';


import { Application } from './declarations';
import logger from './logger';
import middleware from './middleware';
import services from './services';
import appHooks from './app.hooks';
import channels from './channels';
import { HookContext as FeathersHookContext } from '@feathersjs/feathers';
import sequelize from './sequelize';
import mssqlSequelize from './mssqlsequelize';
// Don't remove this comment. It's needed to format import lines nicely.

//const sslRedirect: any = require('heroku-ssl-redirect');
const nocache = require('nocache');
const onHeaders = require('on-headers');

const app: Application = express(feathers());
export type HookContext<T = any> = { app: Application } & FeathersHookContext<T>;

// Load app configuration
app.configure(configuration());
// Enable security, CORS, compression, favicon and body parsing
app.use(helmet({
  contentSecurityPolicy: false
}));
app.use(cors());
console.log(sslRedirect);
app.use(sslRedirect())
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
app.use('/', express.static(app.get('public')));
app.use(nocache());

// Set up Plugins and providers
app.configure(express.rest());
app.configure(socketio());

app.configure(sequelize);
app.configure(mssqlSequelize);

// Configure other middleware (see `middleware/index.ts`)
app.configure(middleware);
// Set up our services (see `services/index.ts`)
app.configure(services);
// Set up event channels (see channels.ts)
app.configure(channels);

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger } as any));
// install it as a middleware
app.use((req, res, next) => {
    // listen for the headers event
    onHeaders(res, () => {
        res.removeHeader('ETag');
    });
});

app.hooks(appHooks);

export default app;
