
import axios from 'axios';
import logger from './logger';
import * as crypto from 'crypto';
import { StreamComment, VideoResponse, WhitelistedUserResponse } from './models/facebook.model';
import { BadRequest } from '@feathersjs/errors';

export class FbPageResponse {
  public access_token: string = '';
  public has_transitioned_to_new_page_experience: boolean = false;
  public name: string = '';
  public id: string = '';
  public email: string = '';
  public tasks: string[] = [];
  public expires_in: number = 0;
  public token: string = '';
  public hashToken: string = '';
}

export class FbUserResponse {
  public access_token: string = '';
  public hashToken: string = '';
  public name: string = '';
  public id: string = '';
  public email: string = '';
  public first_name: string = '';
  public last_name: string = '';
  public picture?: any;
}


export class FacebookGraphAPIClient {

  private static appSecret: string;

  public GRAPH_API_URL = 'https://graph.facebook.com/v10.0';

  constructor(secret: string) { 
    FacebookGraphAPIClient.appSecret = secret;
  }

  public static createAppSecretProof(authToken: string): string {

    const hmac = crypto.createHmac('sha256', FacebookGraphAPIClient.appSecret);
    hmac.update(authToken);
    return hmac.digest('hex');
  }

  public async getListOfPages(fbId: string, userAuthToken: string) {
    logger.info(`[FacebookGraphAPIClient] Get pages of Facebook user ID: ${fbId}`);
    const appSecretProof = FacebookGraphAPIClient.createAppSecretProof(userAuthToken);
    logger.info(`https://graph.facebook.com/${fbId}/accounts?fields=access_token,name,id,tasks,has_transitioned_to_new_page_experience&access_token=${userAuthToken}&appsecret_proof=${appSecretProof}`);
    const listOfPages = await axios.get(`https://graph.facebook.com/${fbId}/accounts?fields=access_token,name,id,tasks,has_transitioned_to_new_page_experience&access_token=${userAuthToken}&appsecret_proof=${appSecretProof}`).then(response => {
      let pages = response.data.data as FbPageResponse[];
      logger.info(`[FacebookGraphAPIClient] Get pages result: `, pages);
      return pages;
    }).catch(error => {
      logger.error(`[FacebookGraphAPIClient] Get pages error`, error.response.data);
      throw new BadRequest('Bad Facebook Request', error.response.data); // Pass the Facebook error details
    });
    for(let i = 0; i< listOfPages.length; i++){
      listOfPages[i].hashToken = FacebookGraphAPIClient.createAppSecretProof(listOfPages[i].access_token);
    }
    return listOfPages;
  }


  public async getEmail(fbId: string, userAuthToken: string): Promise<string> {
    logger.debug(`[FacebookGraphAPIClient] Get email of Facebook user ID: ${fbId}`);
    const appSecretProof = FacebookGraphAPIClient.createAppSecretProof(userAuthToken);
    const isValidInfo = await axios.get(`https://graph.facebook.com/me?access_token=${userAuthToken}&fields=id,name,email&appsecret_proof=${appSecretProof}`).then(response => {
      let user = response.data as FbPageResponse;
      logger.debug(`[FacebookGraphAPIClient] Get email result: `, user);
      if (user.id == fbId) {
        return user.email;
      }
      return '';
    }).catch(error => {
      logger.error(`[FacebookGraphAPIClient] Get email error: `, error.response.data);
      return '';
    });
    return isValidInfo;
  }


  public async getLongLive(appId: string, authToken: string): Promise<FbUserResponse> {
    logger.debug(`[FacebookGraphAPIClient] Get long live token for client ID ${appId} with auth token ${authToken}`);
    const appSecretProof = FacebookGraphAPIClient.createAppSecretProof(authToken);
    const url = `${this.GRAPH_API_URL}/oauth/access_token?grant_type=fb_exchange_token&client_id=${appId}&client_secret=${FacebookGraphAPIClient.appSecret}&fb_exchange_token=${authToken}&appsecret_proof=${appSecretProof}`;
    const longLiveToken = await axios.get(url).then(response => {
      let accessToken = response.data as FbPageResponse;
      logger.debug(`[FacebookGraphAPIClient] Get long live token result: `, accessToken);
      return accessToken.access_token;
    }).catch(error => {
      logger.error(`[FacebookGraphAPIClient] Get long live token error: `, error.response.data);
      throw new BadRequest('Bad Facebook Request', error.response.data); // Pass the Facebook error details
    });

    let details = await this.getDetails(longLiveToken);
    details.access_token = longLiveToken;
    details.hashToken = FacebookGraphAPIClient.createAppSecretProof(longLiveToken);

    return details;
  }

  public async getDetails(userAuthToken: string): Promise<FbUserResponse> {
    logger.debug(`[FacebookGraphAPIClient] Get user details with auth token ${userAuthToken}`);
    const appSecretProof = FacebookGraphAPIClient.createAppSecretProof(userAuthToken);
    const userInfo = await axios.get(`https://graph.facebook.com/me?access_token=${userAuthToken}&fields=id,name,email,picture,first_name,last_name&appsecret_proof=${appSecretProof}`).then(response => {
      let user = response.data as FbUserResponse;
      logger.debug(`[FacebookGraphAPIClient] Get user details result: `, user);
      return user;
    }).catch(error => {
      logger.error(`[FacebookGraphAPIClient] Get user details error: `, error.response.data);
      throw new BadRequest('Bad Facebook Request', error.response.data); // Pass the Facebook error details
    });
    return userInfo;
  }

  public async getRoles(appId: string, accessToken: string): Promise<WhitelistedUserResponse> {
    logger.debug(`[FacebookGraphAPIClient] Get roles for app ID ${appId} with access token ${accessToken}`);
    const appSecretProof = FacebookGraphAPIClient.createAppSecretProof(accessToken);
    const dataInfo = await axios.get(`${this.GRAPH_API_URL}/${appId}/roles?access_token=${accessToken}&appsecret_proof=${appSecretProof}`).then(response => {
      let data = response.data as WhitelistedUserResponse;
      logger.debug(`[FacebookGraphAPIClient] Get roles result: `, data);
      return data;
    }).catch(error => {
      logger.error(`[FacebookGraphAPIClient] Get roles error: `, error.response.data);
      throw new BadRequest('Bad Facebook Request', error.response.data); // Pass the Facebook error details
    });
    return dataInfo;
  }

  public async getLiveVideos(pageId: string, pageAuthToken: string): Promise<VideoResponse> {
    logger.debug(`[FacebookGraphAPIClient] Get live videos of page ID ${pageId} with access token ${pageAuthToken}`);
    const appSecretProof = FacebookGraphAPIClient.createAppSecretProof(pageAuthToken);
    const url = `${this.GRAPH_API_URL}/${pageId}/live_videos?access_token=${pageAuthToken}&appsecret_proof=${appSecretProof}&fields=status,id,permalink_url`;
    return await axios.get(url).then(response => {
      let data = response.data as VideoResponse;
      logger.debug(`[FacebookGraphAPIClient] Get live videos result: `, data);
      return data;
    }).catch(error => {
      logger.error(`[FacebookGraphAPIClient] Get live videos error: `, error.response.data);
      throw new BadRequest('Bad Facebook Request', error.response.data); // Pass the Facebook error details
    });
  }

  public async getAllPosts(pageId: string, pageAuthToken: string): Promise<VideoResponse> {
    logger.debug(`[FacebookGraphAPIClient] Get posts of page ID ${pageId} with access token ${pageAuthToken}`);
    const appSecretProof = FacebookGraphAPIClient.createAppSecretProof(pageAuthToken);
    const url = `${this.GRAPH_API_URL}/${pageId}/posts?access_token=${pageAuthToken}&appsecret_proof=${appSecretProof}`;
    return await axios.get(url).then(response => {
      let data = response.data;
      logger.debug(`[FacebookGraphAPIClient] Get posts result: `, data);
      return data;
    }).catch(error => {
      logger.error(`[FacebookGraphAPIClient] Get posts error: `, error.response.data);
      throw new BadRequest('Bad Facebook Request', error.response.data); // Pass the Facebook error details
    });
  }

  public async getAllComments(postId: string, pageAuthToken: string): Promise<StreamComment[]> {
    logger.debug(`[FacebookGraphAPIClient] Get comments of post ID ${postId} with access token ${pageAuthToken}`);
    const appSecretProof = FacebookGraphAPIClient.createAppSecretProof(pageAuthToken);
    const url = `${this.GRAPH_API_URL}/${postId}/comments?access_token=${pageAuthToken}&appsecret_proof=${appSecretProof}`;
    return await axios.get(url).then(response => {
      let comments = response.data.data as StreamComment[];
      logger.debug(`[FacebookGraphAPIClient] Get comments result: `, comments);
      return comments;
    }).catch(error => {
      logger.error(`[FacebookGraphAPIClient] Get comments error: `, error.response.data);
      throw new BadRequest('Bad Facebook Request', error.response.data); // Pass the Facebook error details
    });
  }

  public async replyToComment(message: string, commentId: string, pageAuthToken: string): Promise<any> {
    logger.debug(`[FacebookGraphAPIClient] Reply to comment ID ${commentId} with access token ${pageAuthToken} with message "${message}"`);
    const appSecretProof = FacebookGraphAPIClient.createAppSecretProof(pageAuthToken);
    const url = `${this.GRAPH_API_URL}/${commentId}/comments?access_token=${pageAuthToken}&appsecret_proof=${appSecretProof}`;
    return await axios.post(url, { message }).then(response => {
      logger.debug(`[FacebookGraphAPIClient] Reply to comment success`);
      return response.data as any;
    }).catch(error => {
      logger.error(`[FacebookGraphAPIClient] Reply to comment error: `, error.response.data);
      throw new BadRequest('Bad Facebook Request', error.response.data); // Pass the Facebook error details
    });
  }

  public async deleteComment(commentId: string, pageAuthToken: string): Promise<any> {
    logger.debug(`[FacebookGraphAPIClient] Delete comment ID ${commentId} with access token ${pageAuthToken}`);
    const appSecretProof = FacebookGraphAPIClient.createAppSecretProof(pageAuthToken);
    const url = `${this.GRAPH_API_URL}/${commentId}?access_token=${pageAuthToken}&appsecret_proof=${appSecretProof}`;
    return await axios.delete(url).then(response => {
      logger.debug(`[FacebookGraphAPIClient] Delete comment success`);
      return response.data as any;
    }).catch(error => {
      logger.error(`[FacebookGraphAPIClient] Delete comment error: `, error.response.data);
      throw new BadRequest('Bad Facebook Request', error.response.data); // Pass the Facebook error details
    });
  }

  public async likeComment(commentId: string, pageAuthToken: string): Promise<any> {
    logger.debug(`[FacebookGraphAPIClient] Like comment ID ${commentId} with access token ${pageAuthToken}`);
    const appSecretProof = FacebookGraphAPIClient.createAppSecretProof(pageAuthToken);
    const url = `${this.GRAPH_API_URL}/${commentId}/likes?access_token=${pageAuthToken}&appsecret_proof=${appSecretProof}`;
    return await axios.get(url).then(response => {
      logger.debug(`[FacebookGraphAPIClient] Like comment success`);
      return response.data as any;
    }).catch(error => {
      logger.error(`[FacebookGraphAPIClient] Like comment error: `, error.response.data);
      throw new BadRequest('Bad Facebook Request', error.response.data); // Pass the Facebook error details
    });
  }

  public async createSubscribedApp(pageId:string, pageAuthToken: string): Promise<any> {
    logger.debug(`[FacebookGraphAPIClient] Creating subscribed app with pageId: ${pageId} and access token ${pageAuthToken}`);
    const appSecretProof = FacebookGraphAPIClient.createAppSecretProof(pageAuthToken);
    const subscribedFields = 'feed';
    const url = `${this.GRAPH_API_URL}/${pageId}/subscribed_apps?subscribed_fields=${subscribedFields}&access_token=${pageAuthToken}&appsecret_proof=${appSecretProof}`;
    return await axios.post(url, { }).then(response => {
      logger.debug(`[FacebookGraphAPIClient] Created subscribed app success`);
      return response.data as any;
    }).catch(error => {
      logger.error(`[FacebookGraphAPIClient] Created subscribed app error: `, error.response.data);
      throw new BadRequest('Bad Facebook Request', error.response.data); // Pass the Facebook error details
    });
  }

  public async getSubscribedApps(pageId:string, pageAuthToken: string): Promise<any> {
    logger.debug(`[FacebookGraphAPIClient] Get subscribed app with pageId: ${pageId} and access token ${pageAuthToken}`);
    const appSecretProof = FacebookGraphAPIClient.createAppSecretProof(pageAuthToken);
    const subscribedFields = 'feed';
    const url = `${this.GRAPH_API_URL}/${pageId}/subscribed_apps?access_token=${pageAuthToken}&appsecret_proof=${appSecretProof}`;
    return await axios.get(url).then(response => {
      logger.debug(`[FacebookGraphAPIClient] Get subscribed app success`);
      return response.data as any;
    }).catch(error => {
      logger.error(`[FacebookGraphAPIClient] Get subscribed app error: `, error.response.data);
      throw new BadRequest('Bad Facebook Request', error.response.data); // Pass the Facebook error details
    });
  }

  
  public async replyToMessage(message: string, recipientId: string, pageAuthToken: string, isResponse: boolean): Promise<any> {
    logger.debug(`[FacebookGraphAPIClient] Reply to recipient ID ${recipientId} with access token ${pageAuthToken} with message "${message}"`);
    const appSecretProof = FacebookGraphAPIClient.createAppSecretProof(pageAuthToken);
    const url = `${this.GRAPH_API_URL}/me/messages?access_token=${pageAuthToken}&appsecret_proof=${appSecretProof}`;
    return await axios.post(url, { 
        'messaging_type': isResponse?'RESPONSE':'UPDATE',
        'recipient': { 'id': recipientId },
        'message': { 'text': message } 
      }).then(response => {
        logger.debug(`[FacebookGraphAPIClient] Reply to comment success`);
        return response.data as any;
      }).catch(error => {
        logger.error(`[FacebookGraphAPIClient] Reply to comment error: `, error.response.data);
        throw new BadRequest('Bad Facebook Request', error.response.data); // Pass the Facebook error details
      });
  }


}