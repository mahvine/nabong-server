import { FbError } from "./facebook.model"

/**
 * The error response data for our APIs. This can be extracted from error.data in the error
 * callback for HTTP requests.
 */
export class ErrorResponseData {
  constructor(
    public code: number,
    public message: string,
    public facebookError?: FbError
  ) { }
}

/*
  1000 - Request-related
  2000 - Internal
  3000 - Database
  4000 - Facebook
  5000 - Other 3rd party
  9000 - General
*/

export enum ErrorCodes {
  FacebookError = 4001
}

/**
 * Returned when we did an internal Facebook API call which then returned an error.
 */
export class FacebookErrorResponseData extends ErrorResponseData {
  constructor(message: string, facebookError: FbError) {
    super(4001, message, facebookError);
  }
}
