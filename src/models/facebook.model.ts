export class VideoResponse {
  public data: Video[] = [];
  public paging?: Pagination;
}

export class Video {
  public id?: string;
  public name?: string;
  public status?: string;
  public permalink_url?: string;
}

export class StreamComment {
  public from?: User;
  public message?: string;
  public id?: string;
  public keyword?: string;
  public trimmedMessage?: string;
}

export class User {
  public name?: string;
  public id?: string;
  public role?: string;
}
export class Pagination {
  public next?: string;
  public previous?: string;
}
export class WhitelistedUserResponse {
  public data: User[] = [];
  public paging?: Pagination;
}

export class FbError {
  public message?: string;
  public type?: string;
  public code?: number;
  public error_subcode?: number;
  public error_user_title?: string;
  public error_user_msg?: string;
  public fbtrace_id?: string;
}