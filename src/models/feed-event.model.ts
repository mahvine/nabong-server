export class PageNotif {
    public object: string = '';
    public entry: WebhookEvent[] = [];
}

export class WebhookEvent {
    public id:string;
    public changes: FeedEvent[];
    public messaging: MessageEvent[];
    constructor() {
        this.id ='';
        this.changes = [];
        this.messaging = []
    }
}

export class MessageEvent {
    public sender?: FbUser ;
    public recipient?: FbUser;
    public timestamp?: number;
    public message?: MessageDetail;
}

export class MessageDetail {
    public mid?: string;
    public text?: string;
}


export class FeedEvent {
    public value: FeedDetail;
    public field: string;
    constructor(){
        this.field = '';
        this.value = <FeedDetail>{};
    }

    get commentId() {
        return this.value.comment_id;
    }
    get message() {
        return this.value.message;
    }

    get  ownerId() {
        return this.value.from.id;
    }
    
    isNewComment() {
        return this.value.isNewComment() && this.value.isNotReply();
    }
}

export class FeedDetail {
    public item: string = '';
    public verb: string = '';
    public comment_id: string = '';
    public parent_id: string = '';
    public post_id: string = '';
    public message: string = '';
    public from: FbUser = new FbUser();
    
    isNewComment() {
        return this.item =='comment' && this.verb =='add';
    }

    isNotReply() {
        return this.parent_id === this.post_id;
    }

}

export class FbUser {
    public id: string = '';
    public name: string = '';

}