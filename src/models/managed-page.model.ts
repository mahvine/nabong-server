
export class ManagedPageModel {
    public id: string = '';
    public name: string = '';
    public userId: string = '';
    public email: string= '';
    public accessToken: string = '';
    public hasTransitionedToNpe: boolean = false;
    public hashToken: string = '';
    public roles: any[] = [];
    constructor(){

    }
}
