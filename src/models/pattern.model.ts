// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/lib/hooks';

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const pattern = sequelizeClient.define('pattern', {
    patternRegex: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'pattern_regex' 
    },
    typeName: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'pattern_type' 
    },
    fieldNames: {
      type: DataTypes.ARRAY(DataTypes.TEXT),
      allowNull: false,
      field: 'field_names' 
    },
    pageOwnerId: {
      type: DataTypes.STRING,
      field: 'page_owner_id' 
    },
    mappings: {
      type: DataTypes.JSON,
      field: 'field_mapping'
    }
  }, {
    hooks: {
      beforeCount(options: any): HookReturn {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (pattern as any).associate = function (models: any): void {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return pattern;
}


export class PatternModel {
  public id:number=0;
  public patternRegex: string='';
  public typeName: string='';
  public fieldNames: string[] = [];
  public pageOwnerId?: string;
  public mappings: any = {};
}

