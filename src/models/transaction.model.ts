// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/lib/hooks';

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const transaction = sequelizeClient.define('transaction', {
    id: {
          type: DataTypes.BIGINT,
          primaryKey: true,
          autoIncrement: true
    },
    message: {
      type: DataTypes.STRING,
      allowNull: false
    },
    pageId: {
      type: DataTypes.STRING,
      allowNull: false,
      field:'page_id'
    },
    senderId: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'sender_id'
    },
    metadata: {
      type: DataTypes.JSON,
      allowNull: true,
      field: 'txn_metadata'
    },
    transactionType: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'txn_type'
    },
    transactionStatus: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'txn_status'
    },
    wpcId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'wpc_id'
    },
    hasBlanks: {
        type: DataTypes.VIRTUAL
    },
    replyMessage: {
        type: DataTypes.VIRTUAL
    }
  }, {
    hooks: {
      beforeCount(options: any): HookReturn {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (transaction as any).associate = function (models: any): void {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return transaction;
}

export class TransactionModel {
  public senderId: string = '';
  public id?:number;
  public message?: string;
  public pageId?: string;
  public metadata?: any = {};
  public transactionType?: string = '';
  public transactionStatus?: string='';
  public hasBlanks?: boolean= false;
  public replyMessage?: string;
  public wpcId?:number;
}