// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/lib/hooks';

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('mssqlSequelizeClient');
  const wpcTxn = sequelizeClient.define('tblTransactions', {
    autoId: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      allowNull: false,
      field: 'autoID' ,
      autoIncrement: true
    },
    modeName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Mode' 
    },
    typeName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Type' 
    },
    username: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Username' 
    },
    amount: {
      type: DataTypes.DECIMAL(8,2),
      allowNull: true,
      field: 'Amount' 
    },
    transactionDetails: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'TransactionDetails' 
    },
    status: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Status' 
    },
    senderId: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'SenderID' 
    },
  }, {
    hooks: {
      beforeCount(options: any): HookReturn {
        options.raw = true;
      }
    },
    timestamps: false,
    // If don't want createdAt
    createdAt: false,
    // If don't want updatedAt
    updatedAt: false,

  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (wpcTxn as any).associate = function (models: any): void {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return wpcTxn;
}


export class WpcTableTxnModel {
  public autoId:number=0;
  public modeName: string='';
  public typeName: string='';
  public username: string='';
  public amount?: number=0;
  public transactionDetails: string='';
  public status: string='';
  public senderId: string='';
}

