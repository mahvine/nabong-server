import { Sequelize } from 'sequelize';
import { Application } from './declarations';

export default function (app: Application): void {
  let connectionString = '';
  if(app.get('mssqlUser')) {
    connectionString = 'mssql://'+app.get('mssqlUser')+':'+app.get('mssqlPass')+'@'+app.get('mssqlHost')+':'+app.get('mssqlPort')+'/'+app.get('mssqlSchema');

  } else {
    connectionString = app.get('mssql');
  }
  let sequelize:Sequelize;
  if(app.get('mssqlUser')){
    sequelize = new Sequelize(app.get('mssqlSchema'), app.get('mssqlUser'), app.get('mssqlPass'), {
      dialect: 'mssql',
      logging: false,
      host: app.get('mssqlHost'),
      port: app.get('mssqlPort') as number,
      define: {
        freezeTableName: true
      }
    });

  } else {
    sequelize = new Sequelize(connectionString, {
      dialect: 'mssql',
      logging: false,
      define: {
        freezeTableName: true
      }
    });
  }
  const oldSetup = app.setup;

  app.set('mssqlSequelizeClient', sequelize);

  app.setup = function (...args): Application {
    const result = oldSetup.apply(this, args);

    // Set up data relationships
    const models = sequelize.models;
    Object.keys(models).forEach(name => {
      if ('associate' in models[name]) {
        (models[name] as any).associate(models);
      }
    });

    // Sync to the database
    app.set('mssqlSequelizeSync', sequelize.sync());

    return result;
  };

}
