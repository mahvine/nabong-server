import { Sequelize } from 'sequelize';
import { Application } from './declarations';

export default function (app: Application): void {
  const connectionString = app.get('postgres');
  
  const postgresSsl = JSON.parse(app.get('postgresSsl')) as boolean;
  let options:any = {
    dialect: 'postgres',
    logging: false,
    define: {
      freezeTableName: true
    }
  };
  if(postgresSsl) {
	options['ssl'] = true;
	options['dialectOptions'] = {
		ssl: {
			rejectUnauthorized: false
		}
	};
  }
  const sequelize = new Sequelize(connectionString, options);
  const oldSetup = app.setup;

  app.set('sequelizeClient', sequelize);

  app.setup = function (...args): Application {
    const result = oldSetup.apply(this, args);

    // Set up data relationships
    const models = sequelize.models;
    Object.keys(models).forEach(name => {
      if ('associate' in models[name]) {
        (models[name] as any).associate(models);
      }
    });

    // Sync to the database
    app.set('sequelizeSync', sequelize.sync());

    return result;
  };
}
