import { BadRequest, NotAuthenticated, Unavailable } from '@feathersjs/errors';
import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { FacebookGraphAPIClient, FbUserResponse } from '../../facebook-graph-api.service';
import logger from '../../logger';

interface Data { }

interface ServiceOptions { }

export class Custom implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  graphClient: FacebookGraphAPIClient;
  appId: string = '';
  appSecret: string = '';
  appSecretProof: string = '';
  trustedFbIds: string[];

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    this.appId = this.app.get('fbAppId');
    this.appSecret = this.app.get('fbAppSecret');
    const ids = this.app.get('trustedFbIds') as string || '';
    this.trustedFbIds = ids.split(',');
    this.graphClient = new FacebookGraphAPIClient(this.appSecret);
  }

  async find(params?: Params): Promise<FbUserResponse> {
    if (params) {
      let queries = { ...params.query } || {};
      let headers = { ...params.headers } || {};
      logger.info('get long live access token query data:', queries);
      const authToken = queries['authToken'];
      const fbId = queries['fbId'];
      if (authToken) {
        const longLiveToken = await this.graphClient.getLongLive(this.appId, authToken);
        return longLiveToken;
      } else {
        throw new BadRequest('No auth token provided');
      }
    }
    throw new BadRequest('Invalid parameters');
  }

  async get(id: string, params?: Params): Promise<Data> {


    const appsecret_proof = FacebookGraphAPIClient.createAppSecretProof(id);

    logger.info("appId:" + this.appId)
    logger.info("appsecret_proof:" + appsecret_proof)
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  async create(data: Data, params?: Params): Promise<Data> {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return data;
  }

  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }
}
