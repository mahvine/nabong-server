// Initializes the `custom` service on path `/custom`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Custom } from './custom.class';
import hooks from './custom.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'api/custom': Custom & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/api/custom', new Custom(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('api/custom');

  service.hooks(hooks);
}
