import { Application } from '../declarations';
import pattern from './pattern/pattern.service';
import transaction from './transaction/transaction.service';
import custom from './custom/custom.service';
import pages from './pages/pages.service';
import webhook from './webhook/webhook.service';
import page from './page/page.service';
import wpcTxn from './wpc-txn/wpc-txn.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application): void {
  app.configure(pattern);
  app.configure(transaction);
  app.configure(custom);
  app.configure(pages);
  app.configure(webhook);
  app.configure(page);
  app.configure(wpcTxn);
}
