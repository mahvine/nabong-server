// Initializes the `page` service on path `/api/page`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Page } from './page.class';
import createModel from '../../models/page.model';
import hooks from './page.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'api/page': Page & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/api/page', new Page(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('api/page');

  service.hooks(hooks);
}
