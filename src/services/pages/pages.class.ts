import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import logger from '../../logger';
import { ManagedPageModel } from '../../models/managed-page.model';
import { FacebookGraphAPIClient, FbPageResponse } from '../../facebook-graph-api.service';
import { BadRequest, Unavailable } from '@feathersjs/errors';
import { FacebookErrorResponseData } from '../../models/error-response.model';
import { log } from 'winston';
import createModel from '../../models/page.model';

interface Data { }

interface ServiceOptions { }
const MANAGEDPAGES = 'managedPages';

export class Pages implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  graphClient: FacebookGraphAPIClient;
  appId: string = '';
  appSecret: string = '';
  appSecretProof: string = '';
  PAGEMODEL: any;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    this.appId = this.app.get('fbAppId');
    this.appSecret = this.app.get('fbAppSecret');
    this.graphClient = new FacebookGraphAPIClient(this.appSecret);
    this.PAGEMODEL = createModel(app);
  }

  async find(params?: Params): Promise<ManagedPageModel[]> {
    if (params) {
      let queries = { ...params.query } || {};
      const headers = { ...params.headers } || {};
      const authToken = headers['x-authtoken'];
      logger.info('[ManagedPage] Get list of pages query data: ', queries);
      const fbId = queries['fbId'];
      const userEmail = queries['userEmail'];

      if (authToken) {
        let fbPages: FbPageResponse[] = [];
        try {
          fbPages = await this.graphClient.getListOfPages(fbId, authToken);
          logger.info('fbPages');
          logger.info(fbPages);
        } catch (error:any) {
          let errorMessage = 'Could not get list of pages from Facebook at this time.';
          let facebookError = error.data?.error; // Extract Facebook error
          throw new Unavailable(errorMessage, new FacebookErrorResponseData(errorMessage, facebookError));
        }

        if (fbPages.length > 0) {
          const pages: ManagedPageModel[] = fbPages.map(page => {
            return {
              ...new ManagedPageModel(),
              id: page.id,
              name: page.name,
              userId: fbId,
              accessToken: page.access_token,
              email: userEmail,
              hashToken: page.hashToken,
              hasTransitionedToNpe: page.has_transitioned_to_new_page_experience
            }
          });
          //Save to database 
          logger.info('saving pages...');
          const result = await this.PAGEMODEL.bulkCreate(pages, 
            {
                fields:['id', 'name', 'userId', 'email', 'hashToken','hasTransitionedToNpe'] ,
                updateOnDuplicate: ['id'] 
            } );
          logger.info('upsert pages successful');

          logger.info('[ManagedPage] Get managed pages response: ', pages);

          return pages;
        } else {
          return [];
        }
      } else {
        throw new Error('No auth token found');
      }
    } else {
      throw new BadRequest('Invalid parameters');
    }
  }

  // Get page by ID
  async get(pageId: string, params?: Params): Promise<ManagedPageModel> {
    return new ManagedPageModel();
  }

  async create(data: Data, params?: Params): Promise<ManagedPageModel> {
    return Object.assign(new ManagedPageModel(), data);
  }

  // Stub -- Consider removal
  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  // Stub -- Consider removal
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async remove(pageId: string, params?: Params): Promise<Data> {
    return pageId;
  }
}

export class ManagedPageRequest {
  public pageId: string = '';
  public discordId: string = '';
  constructor() { }
}
