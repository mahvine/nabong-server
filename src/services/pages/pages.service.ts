// Initializes the `pages` service on path `/api/pages`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Pages } from './pages.class';
import hooks from './pages.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'api/pages': Pages & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/api/pages', new Pages(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('api/pages');

  service.hooks(hooks);
}
