// Initializes the `pattern` service on path `/api/pattern`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Pattern } from './pattern.class';
import createModel from '../../models/pattern.model';
import hooks from './pattern.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'api/pattern': Pattern & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/api/pattern', new Pattern(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('api/pattern');

  service.hooks(hooks);
}
