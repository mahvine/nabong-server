import { Id, NullableId, Params } from '@feathersjs/feathers';
import { Service, SequelizeServiceOptions } from 'feathers-sequelize';
import { Application } from '../../declarations';
import createWpcTxnModel,{WpcTableTxnModel} from '../../models/wpc-txn.model';
import createTxnModel,{TransactionModel} from '../../models/transaction.model';

export class Transaction extends Service {

  
  WPCTXNMODEL: any;
  TXNMODEL: any;
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<SequelizeServiceOptions>, app: Application) {
    super(options);
    this.WPCTXNMODEL = createWpcTxnModel(app);
    this.TXNMODEL = createTxnModel(app);
  }

  async update (id: number, data: TransactionModel, params: Params) {    
    const txn = await this.TXNMODEL.findByPk(id) as TransactionModel;
    if(txn.wpcId){
      await this.WPCTXNMODEL.update({ status: data.transactionStatus }, {
        where: {
          autoId: txn.wpcId
        }
      });
    }
    return super.update(id, data, params);
  }  


}
