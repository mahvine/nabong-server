import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import logger from '../../logger';
import axios from 'axios';
import { MessageEvent, PageNotif, WebhookEvent } from '../../models/feed-event.model';
import { FacebookGraphAPIClient } from '../../facebook-graph-api.service';
import createPageModel from '../../models/page.model';
import createPatternModel, {PatternModel} from '../../models/pattern.model';
import createWpcTxnModel,{WpcTableTxnModel} from '../../models/wpc-txn.model';
import createTxnModel,{TransactionModel} from '../../models/transaction.model';
import { ManagedPageModel } from '../../models/managed-page.model';

interface Data {
  pageAccessToken?:string;
  id:NullableId
}

interface ServiceOptions {}

const modeMapping:any = {
  'CASH IN': 'DEPOSIT',
  'CASH OUT': 'WITHDRAW'
}
export class Webhook implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  appId: string = '';
  appSecret: string = '';
  appSecretProof: string = '';
  graphClient: FacebookGraphAPIClient;
  socketio: any = undefined;
  PAGEMODEL: any;
  TXNMODEL: any;
  PATTERNMODEL: any;
  WPCTXNMODEL: any;
  DEFAULT_REPLY: string ='';
  DEBT_REPLY: string='';

  constructor (options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    this.appSecret = this.app.get('fbAppSecret');
    this.graphClient = new FacebookGraphAPIClient(this.appSecret);
    this.PAGEMODEL = createPageModel(app);
    this.TXNMODEL = createTxnModel(app);
    this.PATTERNMODEL = createPatternModel(app);
    this.WPCTXNMODEL = createWpcTxnModel(app);
    this.DEFAULT_REPLY = this.app.get('defaultReply') ||'Processing.. please give it 2 minutes to reflect on your account';
    this.DEBT_REPLY = this.app.get('debtReply') || 'Request sent for approval';
  }

  //async find (params?: Params): Promise<Data[] | Paginated<Data>> {
  async find (params: Params): Promise<any> {
    if(params){
      logger.info('Web hook invoked');
      let queries = {...params.query } || {};
      logger.info('query data:',queries);
      let challenge:number = parseInt(queries['hub.challenge']);
      let mode = queries['hub.mode'];
      let token = queries['hub.verify_token'];
      if(token !== 'MBaaSTesting'){
        logger.error('invalid token');
        throw new Error("Invalid params");
      }
      return challenge;
    }
    throw new Error("Invalid params");
  }

  async get (pageId: string, params?: Params): Promise<any> {
    
    if (params) {
      let queries = { ...params.query } || {};
      const headers = { ...params.headers } || {};
      const authToken = queries['x-authtoken'];
      return await this.graphClient.getSubscribedApps(pageId, authToken);
    } else {
      return {
        pageId, text: `Page access token not set`
      };
    }
  }

  async create (data: any, params?: Params): Promise<any> {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }
    const notif = data as PageNotif;
    const webhookEvent = notif.entry[0];
    const page = await this.PAGEMODEL.findByPk(webhookEvent.id) as ManagedPageModel;
    return Promise.all(webhookEvent.messaging.map(messageEvent => {
      this.processChanges(messageEvent, webhookEvent, page)
    }));
  }

  async processChanges(messageEvent: MessageEvent, webhookEvent: WebhookEvent, page: ManagedPageModel) {

    logger.info(`Message received :${webhookEvent.id} from ${messageEvent.sender?.id} = ${messageEvent.message?.text}`);
    const patterns = await this.PATTERNMODEL.findAll() as PatternModel[];
    const message = messageEvent.message?.text?.trim();
    if(message) {
      let txn = this.parseMessage(message, {...new TransactionModel(),
        message,
        senderId: messageEvent.sender?.id || '',
        pageId: messageEvent.recipient?.id
      }, patterns);
        
      if(txn.transactionType && txn.transactionStatus && !txn.hasBlanks) {      
        logger.info('Saving transaction');
        txn.message = message.replace('\n','');
        const wpcTxn  = this.convertToWpcTxn(txn);
        
        if(this.WPCTXNMODEL)
        await this.WPCTXNMODEL.create(wpcTxn);
        txn.wpcId = wpcTxn.autoId
        await this.TXNMODEL.create(txn);
        
        let replyMessage = 
        logger.info('replying transaction');
        this.graphClient.replyToMessage('Thanks for messaging', messageEvent.sender?.id || '', page.accessToken,false);
      } else if(txn.hasBlanks){
        this.graphClient.replyToMessage('Invalid format for '+txn.transactionType+' detected blank field', messageEvent.sender?.id || '', page.accessToken,false);
      }else if(txn.transactionType && !txn.transactionStatus){
        this.graphClient.replyToMessage('Invalid format for '+txn.transactionType, messageEvent.sender?.id || '', page.accessToken,false);
      } else {
        this.graphClient.replyToMessage('Invalid transaction po', messageEvent.sender?.id || '', page.accessToken,false);
      }
    }
    
    return 'MESSAGE RECEIVED';
  }

  parseMessage(message: string, txn: TransactionModel, patterns: PatternModel[]){
    
    let fields = message.split('\n').filter( field=> field.trim() !== '');
    let messageProcessed = false;
    let metadata:any = {};
    let amount:number;
    for(let i=0;i<patterns.length; i++){
      const pattern = patterns[i];
      const valid = !messageProcessed && pattern.patternRegex && fields[0].match(new RegExp(pattern.patternRegex,'gi'));
      console.log(pattern.typeName+"|"+pattern.patternRegex+'|'+valid);
      if(!messageProcessed && pattern.patternRegex && fields[0].match(new RegExp(pattern.patternRegex,'gi'))){ 
        txn.transactionType = pattern.typeName;
        fields.splice(0,1); // remove type;
        if(fields.length === pattern.fieldNames.length){
          for(var j=0;j<pattern.fieldNames.length;j++){
            var fieldKey = pattern.fieldNames[j];
            var regex= new RegExp(pattern.mappings[fieldKey],'gi');
            metadata[fieldKey] = fields[j].replace(regex, '').trim();
            if(metadata[fieldKey].length === 0){
              txn.hasBlanks = true;
            }
            if(fieldKey==='amount'){
              amount = parseFloat(metadata[fieldKey].replace(/,/g, ''));;
              metadata[fieldKey] = amount;
              console.log(amount);
            }
          }
          txn.metadata = metadata;
          txn.transactionStatus = 'PENDING';

          messageProcessed = true;
        } 
      }
    }
    return txn;
  }
  

  convertToWpcTxn(txn:TransactionModel): WpcTableTxnModel{
    let mode = txn?.transactionType || '';
    if(mode && modeMapping[mode]!==undefined){
      mode = modeMapping[mode];
    }
    let type = mode;
    let details = mode;
    let username = txn.metadata.username;
    if(mode === 'DEPOSIT'){
      if(txn.metadata.gcash){
        type = 'GCASH';
        details = txn.metadata.gcash
      }
    } else if(mode === 'TRANSFER'){
      if(txn.metadata.to){
        type = txn.metadata.to;
        username = txn.metadata.from;
      }
    } else if (mode === 'WITHDRAW') {
      if(txn.metadata.bank){
        type = txn.metadata.bank;
      }
    } else if (mode === 'BANK') {
      mode = 'DEPOSIT';
      if(txn.metadata.bank){
        details = txn.metadata.bank;
      }
    } else if (mode === 'GCASH') {
      mode = 'DEPOSIT';
      if(txn.metadata.refno){
        details = txn.metadata.refno;
      }
    } else if (mode === 'DEBT') {
      mode = 'DEPOSIT';
    }
    let amount:number = 0;
    if(txn.metadata.amount) {
      amount = txn.metadata.amount;
    }

    return {
      ...new WpcTableTxnModel(),
      modeName: mode,
      typeName: type,
      amount: amount,
      username: username,
      status: txn.transactionStatus || 'NEW',
      senderId: txn.senderId,
      transactionDetails: details
    };
  }



  async update (pageId: string, data: Data, params?: Params): Promise<any> {
    
    if (params) {
      let queries = { ...params.query } || {};
      const headers = { ...params.headers } || {};
      const authToken = headers['x-authtoken'];
      return await this.graphClient.createSubscribedApp(pageId, authToken);
    }
    return {pageId};
  }

  async patch (id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async remove (id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }
}
