// Initializes the `webhook` service on path `/webhook`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Webhook } from './webhook.class';
import hooks from './webhook.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'api/webhook': Webhook & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/api/webhook', new Webhook(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('api/webhook');

  service.hooks(hooks);
}
