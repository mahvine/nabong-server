// Initializes the `wpcTxn` service on path `/api/wpcTxn`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { WpcTxn } from './wpc-txn.class';
import createModel from '../../models/wpc-txn.model';
import hooks from './wpc-txn.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'api/wpcTxn': WpcTxn & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/api/wpcTxn', new WpcTxn(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('api/wpcTxn');

  service.hooks(hooks);
}
