import assert from 'assert';
import app from '../../src/app';

describe('\'page\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/page');

    assert.ok(service, 'Registered the service');
  });
});
