import assert from 'assert';
import app from '../../src/app';

describe('\'pattern\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/pattern');

    assert.ok(service, 'Registered the service');
  });
});
